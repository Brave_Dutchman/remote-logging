﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoggingSolution.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LogApplications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogApplications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LogCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    ApplicationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LogCategories_LogApplications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "LogApplications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LogExceptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Hash = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    StackTrace = table.Column<string>(nullable: true),
                    Data = table.Column<string>(nullable: true),
                    InnerExceptionId = table.Column<int>(nullable: true),
                    ApplicationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogExceptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LogExceptions_LogApplications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "LogApplications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LogExceptions_LogExceptions_InnerExceptionId",
                        column: x => x.InnerExceptionId,
                        principalTable: "LogExceptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LogMessages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Hash = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    ApplicationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogMessages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LogMessages_LogApplications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "LogApplications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LogDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Hash = table.Column<string>(nullable: true),
                    EventId = table.Column<int>(nullable: false),
                    EventName = table.Column<string>(nullable: true),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogDatas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LogDatas_LogCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "LogCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LogEntries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LogLevel = table.Column<int>(nullable: false),
                    LoggedOnUtc = table.Column<long>(nullable: false),
                    DataId = table.Column<int>(nullable: true),
                    MessageId = table.Column<int>(nullable: true),
                    ExceptionId = table.Column<int>(nullable: true),
                    AdditionalInfo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogEntries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LogEntries_LogDatas_DataId",
                        column: x => x.DataId,
                        principalTable: "LogDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LogEntries_LogExceptions_ExceptionId",
                        column: x => x.ExceptionId,
                        principalTable: "LogExceptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LogEntries_LogMessages_MessageId",
                        column: x => x.MessageId,
                        principalTable: "LogMessages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LogApplications_Name",
                table: "LogApplications",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_LogCategories_ApplicationId",
                table: "LogCategories",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_LogCategories_Name_ApplicationId",
                table: "LogCategories",
                columns: new[] { "Name", "ApplicationId" },
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_LogDatas_CategoryId",
                table: "LogDatas",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_LogDatas_Hash",
                table: "LogDatas",
                column: "Hash",
                unique: true,
                filter: "[Hash] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_LogEntries_DataId",
                table: "LogEntries",
                column: "DataId");

            migrationBuilder.CreateIndex(
                name: "IX_LogEntries_ExceptionId",
                table: "LogEntries",
                column: "ExceptionId");

            migrationBuilder.CreateIndex(
                name: "IX_LogEntries_MessageId",
                table: "LogEntries",
                column: "MessageId");

            migrationBuilder.CreateIndex(
                name: "IX_LogExceptions_ApplicationId",
                table: "LogExceptions",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_LogExceptions_InnerExceptionId",
                table: "LogExceptions",
                column: "InnerExceptionId");

            migrationBuilder.CreateIndex(
                name: "IX_LogExceptions_Hash_ApplicationId",
                table: "LogExceptions",
                columns: new[] { "Hash", "ApplicationId" },
                unique: true,
                filter: "[Hash] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_LogMessages_ApplicationId",
                table: "LogMessages",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_LogMessages_Hash_ApplicationId",
                table: "LogMessages",
                columns: new[] { "Hash", "ApplicationId" },
                unique: true,
                filter: "[Hash] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LogEntries");

            migrationBuilder.DropTable(
                name: "LogDatas");

            migrationBuilder.DropTable(
                name: "LogExceptions");

            migrationBuilder.DropTable(
                name: "LogMessages");

            migrationBuilder.DropTable(
                name: "LogCategories");

            migrationBuilder.DropTable(
                name: "LogApplications");
        }
    }
}
