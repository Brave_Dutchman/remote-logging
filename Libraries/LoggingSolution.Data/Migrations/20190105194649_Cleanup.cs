﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LoggingSolution.Data.Migrations
{
    public partial class Cleanup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Source",
                table: "LogExceptions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Source",
                table: "LogExceptions",
                nullable: true);
        }
    }
}
