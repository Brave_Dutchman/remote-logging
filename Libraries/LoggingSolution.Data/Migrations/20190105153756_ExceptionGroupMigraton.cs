﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoggingSolution.Data.Migrations
{
    public partial class ExceptionGroupMigraton : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GroupId",
                table: "LogExceptions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "LogExceptionGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogExceptionGroups", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LogExceptions_GroupId",
                table: "LogExceptions",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_LogExceptions_LogExceptionGroups_GroupId",
                table: "LogExceptions",
                column: "GroupId",
                principalTable: "LogExceptionGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LogExceptions_LogExceptionGroups_GroupId",
                table: "LogExceptions");

            migrationBuilder.DropTable(
                name: "LogExceptionGroups");

            migrationBuilder.DropIndex(
                name: "IX_LogExceptions_GroupId",
                table: "LogExceptions");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "LogExceptions");
        }
    }
}
