﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace LoggingSolution.Data
{
    public static class Hasher
    {
        public static string Hash(string toHash)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(toHash);
            using (SHA256 hashstring = SHA256.Create())
            {
                byte[] hash = hashstring.ComputeHash(bytes);
                return Convert.ToBase64String(hash);
            }
        }
    }
}
