﻿using Microsoft.Extensions.Logging;

namespace LoggingSolution.Data
{
    public class LogEntry
    {
        public int Id { get; set; }

        public LogLevel LogLevel { get; set; }

        public long LoggedOnUtc { get; set; }

        public virtual LogData Data { get; set; }

        public virtual LogMessage Message { get; set; }

        public virtual LogException Exception { get; set; }

        public string AdditionalInfo { get; set; }
    }
}
