﻿using System.Collections.Generic;

namespace LoggingSolution.Data
{
    public class LogApplication
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<LogCategory> Categories { get; set; }

        public virtual ICollection<LogException> Exceptions { get; set; }

        public virtual ICollection<LogMessage> Messages { get; set; }
    }
}
