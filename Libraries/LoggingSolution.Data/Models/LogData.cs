﻿using System.Collections.Generic;

namespace LoggingSolution.Data
{
    public class LogData
    {
        public int Id { get; set; }

        public string Hash { get; set; }

        public int EventId { get; set; }

        public string EventName { get; set; }

        public int CategoryId { get; set; }
        public virtual LogCategory Category { get; set; }

        public virtual ICollection<LogEntry> Entries { get; set; }
    }
}
