﻿using System.Collections.Generic;

namespace LoggingSolution.Data
{
    public class LogCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ApplicationId { get; set; }
        public virtual LogApplication Application { get; set; }

        public virtual ICollection<LogData> Datas { get; set; }
    }
}
