﻿using System.Collections.Generic;

namespace LoggingSolution.Data
{
    public class LogExceptionGroup
    {
        public int Id { get; set; }

        public ICollection<LogException> Exceptions { get; set; }
    }
}
