﻿using System.Collections.Generic;

namespace LoggingSolution.Data
{
    public class LogException
    {
        public int Id { get; set; }

        public string Hash { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string Data { get; set; }

        public int? InnerExceptionId { get; set; }
        public LogException InnerException { get; set; }

        public int ApplicationId { get; set; }
        public virtual LogApplication Application { get; set; }

        public int GroupId { get; set; }
        public virtual LogExceptionGroup Group { get; set; }

        public virtual ICollection<LogEntry> Entries { get; set; }
    }
}
