﻿using System.Collections.Generic;

namespace LoggingSolution.Data
{
    public class LogMessage
    {
        public int Id { get; set; }

        public string Hash { get; set; }

        public string Message { get; set; }

        public int ApplicationId { get; set; }
        public virtual LogApplication Application { get; set; }

        public virtual ICollection<LogEntry> Entries { get; set; }
    }
}
