﻿using Microsoft.EntityFrameworkCore;

namespace LoggingSolution.Data
{
    public class LoggingDBContext : DbContext
    {
        public LoggingDBContext(DbContextOptions<LoggingDBContext> options)
            : base(options)
        {  } 

        public DbSet<LogApplication> LogApplications { get; set; }
        
        public DbSet<LogCategory> LogCategories { get; set; }

        public DbSet<LogData> LogDatas { get; set; }

        public DbSet<LogEntry> LogEntries { get; set; }

        public DbSet<LogException> LogExceptions { get; set; }

        public DbSet<LogExceptionGroup> LogExceptionGroups { get; set; }

        public DbSet<LogMessage> LogMessages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<LogData>().HasIndex(nameof(LogData.Hash)).IsUnique();

            modelBuilder.Entity<LogException>().HasIndex(nameof(LogException.Hash), nameof(LogException.ApplicationId)).IsUnique();

            modelBuilder.Entity<LogMessage>().HasIndex(nameof(LogMessage.Hash), nameof(LogMessage.ApplicationId)).IsUnique();

            modelBuilder.Entity<LogCategory>().HasIndex(nameof(LogCategory.Name), nameof(LogCategory.ApplicationId)).IsUnique();

            modelBuilder.Entity<LogApplication>().HasIndex(nameof(LogApplication.Name)).IsUnique();

            modelBuilder.Entity<LogException>().HasOne(x => x.InnerException).WithMany().OnDelete(DeleteBehavior.Restrict);
        }
    }
}
