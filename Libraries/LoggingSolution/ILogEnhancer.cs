﻿using System.Collections.Generic;

namespace LoggingSolution
{
    public interface ILogEnhancer
    {
        void Enhance(Dictionary<string, object> additionalInfo);
    }
}
