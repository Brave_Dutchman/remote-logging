﻿using System;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Internal;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace LoggingSolution
{
    public class RemoteLoggerBackgroundService : BackgroundService
    {
        private readonly IDisposable _optionsReloadToken;
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;
        private readonly RemoteLoggerQueue _remoteLoggerQueue;

        public RemoteLoggerBackgroundService(ILogger<RemoteLoggerBackgroundService> logger, IOptionsMonitor<RemoteLoggerOptions> options, RemoteLoggerQueue remoteLoggerQueue)
        {
            _httpClient = new HttpClient();

            _optionsReloadToken = options.OnChange(ReloadLoggerOptions);
            _logger = logger;
            _remoteLoggerQueue = remoteLoggerQueue;

            ReloadLoggerOptions(options.CurrentValue);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int exceptionCount = 0;

            while (!stoppingToken.IsCancellationRequested)
            {
                LoggerQueueItem current = null;
                try
                {
                    current = await _remoteLoggerQueue.DequeueAsync();
                    if (current == null)
                    {
                        continue;
                    }

                    var postItem = new LogPostItem
                    {
                        LoggerInfo = current.LoggerInfo,
                        LoggedOnUtc = current.LoggedOnUtc,
                        Message = current.Message,
                        EventId = current.EventId.Id,
                        EventName = current.EventId.Name,
                        Exception = Convert(current.Exception),
                        LogLevel = (int)current.LogLevel,
                        AdditionalInfo = current.AdditionalInfo != null ? JsonConvert.SerializeObject(current.AdditionalInfo) : null
                    };

                    var json = JsonConvert.SerializeObject(postItem);
                    StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await _httpClient.PostAsync("api/logging/log", content);

                    if (response.IsSuccessStatusCode)
                    {
                        _logger.LogDebug(InternalSettings.LoggingSolutionEventId, 
                            "Succesfull logged {Name}: {Id}", nameof(LoggerQueueItem), current.Id);
                    }
                    else
                    {
                        _logger.LogError(InternalSettings.LoggingSolutionEventId, 
                            "Response dit not return success: {StatusCode} and reason: {Reason}", response.StatusCode, response.ReasonPhrase);
                    }

                    exceptionCount = 0;
                }
                catch (Exception e) when (e.GetBaseException() is SocketException)
                {
                    exceptionCount++;
                    _logger.LogError(InternalSettings.LoggingSolutionEventId, e, 
                        "SocketException, {Name}: {Id}, Count: {exceptionCount}", nameof(LoggerQueueItem), current?.Id.ToString() ?? "No Current", exceptionCount);
                    Requeue(current);
                }
                catch (Exception e)
                {
                    _logger.LogCritical(InternalSettings.LoggingSolutionEventId, e, "Unknown exception");
                    Requeue(current);
                }

                if (exceptionCount > 0)
                {
                    int waitCount = exceptionCount > 5 ? 5 : exceptionCount;
                    await Task.Delay(waitCount * 2000);
                }
            }
        }

        private ExceptionItem Convert(Exception exception)
        {
            if (exception == null)
            {
                return null;
            }

            var outer = new ExceptionItem();
            var current = outer;
            var currentEx = exception;
            
            do
            {
                current.InnerException = new ExceptionItem
                {
                    Name = currentEx.GetType().FullName,
                    Message = currentEx.Message,
                    Source = currentEx.Source,
                    StackTrace = currentEx.StackTrace,
                    Data = currentEx.Data.Count > 0 ? JsonConvert.SerializeObject(currentEx.Data) : null
                };

                currentEx = currentEx.InnerException;
                current = current.InnerException;
            } while (currentEx != null);

            return outer.InnerException;
        }

        private void Requeue(LoggerQueueItem item)
        {
            _logger.LogInformation(InternalSettings.LoggingSolutionEventId, "requeing {Name}: {Id}", nameof(LoggerQueueItem), item.Id);
            _remoteLoggerQueue.Add(item);
        }

        private void ReloadLoggerOptions(RemoteLoggerOptions options)
        {
            if (options.Url != null)
            {
                if (Uri.TryCreate(options.Url, UriKind.Absolute, out Uri baseAddress))
                {
                    _httpClient.BaseAddress = baseAddress;
                }
            }
        }

        public override void Dispose()
        {
            _optionsReloadToken?.Dispose();
        }

        private static string MessageFormatter(FormattedLogValues state, Exception error)
        {
            return state.ToString();
        }
    }
}
