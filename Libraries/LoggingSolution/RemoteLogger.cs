﻿using System;
using System.Collections.Generic;
using LoggingSolution.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions.Internal;

namespace LoggingSolution
{

    public class RemoteLogger : ILogger
    {
        private readonly RemoteLoggerQueue _loggerQueue;
        private readonly IExternalScopeProvider _scopeProvider;
        private readonly IReadOnlyList<ILogEnhancer> _logEnhancers;

        public RemoteLogger(RemoteLoggerInfo loggerInfo, IReadOnlyList<ILogEnhancer> logEnhancers, LogLevel minLevel, RemoteLoggerQueue loggerQueue, IExternalScopeProvider scopeProvider)
        {
            LoggerInfo = loggerInfo;
            MinLevel = minLevel;
            _loggerQueue = loggerQueue;
            _scopeProvider = scopeProvider;
            _logEnhancers = logEnhancers;
        }

        public string Name => LoggerInfo.CategoryName;

        public RemoteLoggerInfo LoggerInfo { get; }

        public LogLevel MinLevel { get; set; }

        public IDisposable BeginScope<TState>(TState state)
        {
            return _scopeProvider != null ? _scopeProvider.Push(state) : NullScope.Instance;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            if (logLevel == LogLevel.None)
            {
                return false;
            }

            return logLevel >= MinLevel;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var loggedOn = DateTime.UtcNow.Ticks;

            if (!IsEnabled(logLevel))
            {
                return;
            }

            // LoggingSolution event dont log to remote
            if (eventId.Id > 0 && eventId.Equals(InternalSettings.LoggingSolutionEventId))
            {
                return;
            }
            
            var message = formatter(state, exception);
            if (string.IsNullOrEmpty(message) && exception == null)
            {
                return;
            }

            Dictionary<string, object> additionalInfo = null;
            if (_logEnhancers.Count > 0)
            {
                additionalInfo = new Dictionary<string, object>(_logEnhancers.Count);
                _logEnhancers.ForEach(x => x.Enhance(additionalInfo));
            }

            var item = new LoggerQueueItem
            {
                Id = Guid.NewGuid(),
                LoggerInfo = LoggerInfo,
                LogLevel = logLevel,
                EventId = eventId,
                Message = message,
                Exception = exception,
                LoggedOnUtc = loggedOn,
                AdditionalInfo = additionalInfo
            };

            _loggerQueue.Add(item);
        }
    }
}
