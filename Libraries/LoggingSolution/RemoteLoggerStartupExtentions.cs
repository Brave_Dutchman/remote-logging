﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

namespace LoggingSolution
{
    public static class RemoteLoggerStartupExtentions
    {
        public static ILoggingBuilder AddRemoteLogging(this ILoggingBuilder builder)
        {
            builder.Services.AddSingleton<RemoteLoggerQueue>();

            builder.Services.TryAddSingleton<ILoggingSettingsProvider, LoggingSettingsProvider>();

            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<ILoggerProvider, RemoteLoggerProvider>());

            return builder;
        }

        public static ILoggingBuilder AddRemoteLogging(this ILoggingBuilder builder, IConfigurationSection remoteSection, IConfigurationSection loggingSection)
        {
            builder.AddRemoteLogging();
            builder.Services.Configure<RemoteLoggerOptions>(remoteSection);
            builder.Services.Configure<LoggerOptions>(loggingSection);

            return builder;
        }

        public static ILoggingBuilder AddRemoteLogging(this ILoggingBuilder builder, Action<RemoteLoggerOptions> configureRemote, Action<LoggerOptions> configure)
        {
            builder.AddRemoteLogging();
            builder.Services.Configure(configureRemote);
            builder.Services.Configure(configure);

            return builder;
        }
    }
}
