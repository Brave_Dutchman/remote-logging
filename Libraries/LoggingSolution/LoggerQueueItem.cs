﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace LoggingSolution
{
    public class LoggerQueueItem
    {
        public Guid Id { get; set; }

        public RemoteLoggerInfo LoggerInfo { get; set; }

        public LogLevel LogLevel { get; set; }

        public string Message { get; set; }

        public EventId EventId { get; set; }

        public Exception Exception { get; set; }

        public long LoggedOnUtc { get; set; }

        public Dictionary<string, object> AdditionalInfo { get; set; }
    }
}
