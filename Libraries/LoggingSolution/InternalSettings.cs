﻿using Microsoft.Extensions.Logging;

namespace LoggingSolution
{
    internal static class InternalSettings
    {
        public static EventId LoggingSolutionEventId = new EventId(int.MaxValue, nameof(LoggingSolution));
    }
}
