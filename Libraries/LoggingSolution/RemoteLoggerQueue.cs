﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace LoggingSolution
{
    public class RemoteLoggerQueue
    {
        private readonly ConcurrentQueue<LoggerQueueItem> _queue;
        private readonly SemaphoreSlim _semaphoreSlim;

        public RemoteLoggerQueue()
        {
            _semaphoreSlim = new SemaphoreSlim(0);
            _queue = new ConcurrentQueue<LoggerQueueItem>();
        }

        public void Add(LoggerQueueItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            _queue.Enqueue(item);
            _semaphoreSlim.Release();
        }

        public async Task<LoggerQueueItem> DequeueAsync()
        {
            await _semaphoreSlim.WaitAsync();

            _queue.TryDequeue(out var item);
            return item;
        }
    }
}
