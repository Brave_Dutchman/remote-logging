﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace LoggingSolution
{
    public class RemoteLoggerProvider : ILoggerProvider, ISupportExternalScope
    {
        private readonly ConcurrentDictionary<string, RemoteLogger> _loggers;
        private readonly IDisposable _optionsReloadToken;
        private readonly RemoteLoggerQueue _queue;
        private readonly ILoggingSettingsProvider _loggingSettings;
        private readonly List<ILogEnhancer> _logEnhancers;

        private IExternalScopeProvider _scopeProvider;
        private LoggerOptions _options;

        private LogLevel _default;
        
        private RemoteLoggerProvider()
        {
            _loggers = new ConcurrentDictionary<string, RemoteLogger>();
        }

        public RemoteLoggerProvider(RemoteLoggerQueue queue, IEnumerable<ILogEnhancer> logEnhancers, ILoggingSettingsProvider loggingSettings, IOptionsMonitor<LoggerOptions> options) 
            : this()
        {
            _optionsReloadToken = options.OnChange(ReloadLoggerOptions);
            ReloadLoggerOptions(options.CurrentValue);
            _queue = queue;
            _loggingSettings = loggingSettings;
            _logEnhancers = logEnhancers.ToList();
        }

        public ILogger CreateLogger(string categoryName)
        {
            return _loggers.GetOrAdd(categoryName, (c) => new RemoteLogger(LoggerInfo(categoryName), _logEnhancers, _default, _queue, _scopeProvider));
        }

        public void SetScopeProvider(IExternalScopeProvider scopeProvider)
        {
            _scopeProvider = scopeProvider;
        }

        public void Dispose()
        {
            _optionsReloadToken?.Dispose();
        }

        private void ReloadLoggerOptions(LoggerOptions options)
        {
            _options = options;

            var keys = _options.LogLevel;

            _default = LogLevel.Information;
            if (keys.ContainsKey("Default"))
            {
                _default = keys["Default"];
            }

            foreach (var logger in _loggers.Values)
            {
                var matches = keys.Where(x => logger.Name.StartsWith(x.Key)).OrderByDescending(x => x.Key.Length).Take(1).ToArray();
                if (matches.Length == 0)
                {
                    logger.MinLevel = _default;
                }
                else
                {
                    logger.MinLevel = matches[0].Value;
                }
            }
        }

        private RemoteLoggerInfo LoggerInfo(string categoryName)
        {
            return new RemoteLoggerInfo(_loggingSettings.Name, categoryName);
        }
    }
}
