﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace LoggingSolution
{
    public class LoggerOptions
    {
        public LoggerOptions()
        {
            LogLevel = new Dictionary<string, LogLevel>();
        }

        public Dictionary<string, LogLevel> LogLevel { get; set; }
    }
}
