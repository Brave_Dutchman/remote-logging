﻿using System;
using System.Collections.Generic;

namespace LoggingSolution.Internal
{
    internal static class ReadonlyListExtentions
    {
        public static void ForEach<T>(this IReadOnlyList<T> list, Action<T> action)
        {
            foreach (var item in list)
            {
                action(item);
            }
        }
    }
}
