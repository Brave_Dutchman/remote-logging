﻿namespace LoggingSolution
{
    public interface ILoggingSettingsProvider
    {
        string Name { get; }
    }
}