﻿using System.Reflection;

namespace LoggingSolution
{
    public class LoggingSettingsProvider : ILoggingSettingsProvider
    {
        public LoggingSettingsProvider()
        {
            var assembly = Assembly.GetEntryAssembly();
            Name = assembly.FullName.Split(',')[0];
        }

        public string Name { get; }
    }
}
