﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace LoggingSolution.AspNetCore
{
    public class RequestIdEnhancer : ILogEnhancer
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RequestIdEnhancer(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enhance(Dictionary<string, object> additionalInfo)
        {
            var httpContext = _httpContextAccessor?.HttpContext;
            if (httpContext == null)
            {
                return;
            }

            additionalInfo.Add("RequestId", Activity.Current?.Id ?? httpContext.TraceIdentifier);

            if (httpContext.User.Identity.IsAuthenticated)
            {
                additionalInfo.Add("User", $"{httpContext.User.Identity.Name}");
            }
        }
    }
}
