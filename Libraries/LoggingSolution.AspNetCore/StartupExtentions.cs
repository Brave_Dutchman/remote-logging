﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace LoggingSolution.AspNetCore
{
    public static class StartupExtentions
    {
        public static IServiceCollection AddRequestIdLogEnhancer(this IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddEnumerable(ServiceDescriptor.Singleton<ILogEnhancer, RequestIdEnhancer>());

            return services;
        }
    }
}
