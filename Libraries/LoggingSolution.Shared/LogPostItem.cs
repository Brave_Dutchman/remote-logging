﻿namespace LoggingSolution
{
    public class LogPostItem
    {
        public RemoteLoggerInfo LoggerInfo { get; set; }

        public int LogLevel { get; set; }

        public string Message { get; set; }

        public int EventId { get; set; }

        public string EventName { get; set; }

        public ExceptionItem Exception { get; set; }

        public long LoggedOnUtc { get; set; }

        public string AdditionalInfo { get; set; }
    }
}
