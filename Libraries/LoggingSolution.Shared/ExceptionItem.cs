﻿namespace LoggingSolution
{
    public class ExceptionItem
    {
        public string Name { get; set; }

        public string Message { get; set; }

        public string Source { get; set; }

        public string StackTrace { get; set; }

        public string Data { get; set; }

        public ExceptionItem InnerException { get; set; }
    }
}
