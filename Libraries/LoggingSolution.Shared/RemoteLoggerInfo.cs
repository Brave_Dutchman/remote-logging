﻿namespace LoggingSolution
{
    public class RemoteLoggerInfo
    {
        public RemoteLoggerInfo(string applicationName, string categoryName)
        {
            ApplicationName = applicationName;
            CategoryName = categoryName;
        }

        public string ApplicationName { get; }
        public string CategoryName { get; }
    }
}
