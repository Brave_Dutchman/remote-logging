﻿namespace LoggingSolution.Portal.Dtos
{
    public class ExceptionDetailDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }
    }
}
