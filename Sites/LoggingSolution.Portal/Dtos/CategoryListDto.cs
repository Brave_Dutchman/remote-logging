﻿namespace LoggingSolution.Portal.Dtos
{
    public class CategoryListDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ApplicationDto Application { get; set; }
    }
}
