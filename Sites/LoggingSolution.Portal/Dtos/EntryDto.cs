﻿using System;
using Microsoft.Extensions.Logging;

namespace LoggingSolution.Portal.Dtos
{
    public class EntryDto
    {
        private DateTime _cached;

        public int Id { get; set; }

        public int DataId { get; set; }

        public LogLevel LogLevel { get; set; }

        public long LoggedOnUtc { get; set; }

        public string Message { get; set; }

        public EventInfoDto EventInfo { get; set; }

        public ExceptionDetailDto Exception { get; set; }

        public CategoryDto Category { get; set; }

        public string AdditionalInfo { get; set; }

        public bool ShowException => Exception != null;

        public bool ShowMessage => !string.IsNullOrEmpty(Message);

        public bool ShowAdditionalInfo => !string.IsNullOrEmpty(AdditionalInfo);

        public DateTime LoggedOn { get
            {
                if (_cached == default)
                {
                    _cached = new DateTime(LoggedOnUtc, DateTimeKind.Utc).ToLocalTime();
                }

                return _cached;
            }
        }
    }
}
