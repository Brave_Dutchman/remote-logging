﻿namespace LoggingSolution.Portal.Dtos
{
    public class EventInfoDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
