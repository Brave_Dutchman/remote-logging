﻿namespace LoggingSolution.Portal.Dtos
{
    public class ApplicationDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
