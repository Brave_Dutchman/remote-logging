﻿using Newtonsoft.Json;

namespace LoggingSolution.Portal.Dtos.Api
{
    public class ApiExceptionDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string Data { get; set; }

        public ApiExceptionDto InnerException { get; set; }

        [JsonIgnore]
        public int? InnerExceptionId { get; set; }
    }
}