﻿namespace LoggingSolution.Portal.Dtos.Api
{
    public class ApiCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ApiApplicationDto Application { get; set; }
    }
}