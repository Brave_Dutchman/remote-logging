﻿namespace LoggingSolution.Portal.Dtos.Api
{
    public class ApiApplicationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}