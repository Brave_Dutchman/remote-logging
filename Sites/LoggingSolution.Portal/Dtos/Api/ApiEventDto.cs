﻿using System;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace LoggingSolution.Portal.Dtos.Api
{
    public class ApiEventDto
    {
        public int Id { get; set; }
        public LogLevel LogLevel { get; set; }
        public DateTime LoggedOn
        {
            get
            {
                return new DateTime(LoggedOnUtc, DateTimeKind.Utc);
            }
        }
        public string Message { get; set; }
        public string AdditionalInfo { get; set; }
        public EventInfoDto EventInfo { get; set; }
        public ApiCategoryDto Category { get; set; }
        public ApiExceptionDto Exception { get; set; }

        [JsonIgnore]
        public long LoggedOnUtc { get; set; }
        
        [JsonIgnore]
        public int? ExceptionGroupId { get; set; }
    }
}