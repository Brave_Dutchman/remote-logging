﻿namespace LoggingSolution.Portal.Dtos
{
    public class ExceptionDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }

        public string Source { get; set; }

        public string StackTrace { get; set; }

        public string Data { get; set; }

        public ExceptionDetailDto InnerException { get; set; }

        public bool HasInnerException => InnerException != null;
    }
}
