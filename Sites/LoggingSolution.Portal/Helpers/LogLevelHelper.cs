﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace LoggingSolution.Portal.Helpers
{
    public static class LogLevelHelper
    {
        private static readonly Dictionary<LogLevel, string> _logLevelNames = new Dictionary<LogLevel, string>
        {
            { LogLevel.Trace, "Trace" },
            { LogLevel.Debug, "Debug" },
            { LogLevel.Information, "Information" },
            { LogLevel.Warning, "Warning" },
            { LogLevel.Error, "Error" },
            { LogLevel.Critical, "Critical" },
            { LogLevel.None, "None" }
        };

        public static string AsString(LogLevel level)
        {
            return _logLevelNames[level];
        }
    }
}
