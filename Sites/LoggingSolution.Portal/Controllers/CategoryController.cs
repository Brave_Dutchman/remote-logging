﻿using System.Linq;
using System.Threading.Tasks;
using LoggingSolution.Data;
using LoggingSolution.Portal.Dtos;
using LoggingSolution.Portal.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LoggingSolution.Portal.Controllers
{
    public class CategoryController : Controller
    {
        private readonly LoggingDBContext _context;

        public CategoryController(LoggingDBContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var categories = await _context.LogCategories
                .Select(x => new CategoryListDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Application = new ApplicationDto
                    {
                        Id = x.Application.Id,
                        Name = x.Application.Name
                    },
                })
                .ToListAsync();

            CategoryListViewModel vm = new CategoryListViewModel
            {
                Categories = categories
            };

            return View("List", vm);
        }

        public async Task<IActionResult> List(int applicationId)
        {
            var categories = await _context.LogCategories
                .Where(x => x.ApplicationId == applicationId)
                .Select(x => new CategoryDto
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToListAsync();

            categories.ForEach(x => x.ApplicationId = applicationId);

            CategoryListByAppViewModel vm = new CategoryListByAppViewModel
            {
                ApplicationId = applicationId,
                Categories = categories
            };

            return View("ListByApplication", vm);
        }
    }
}
