﻿using System;
using System.Threading.Tasks;
using LoggingSolution.Portal.Services;
using LoggingSolution.Portal.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace LoggingSolution.Portal.Controllers
{
    public class EntryController : Controller
    {
        private readonly LogEntryService _entryService;

        public EntryController(LogEntryService entryService)
        {
            _entryService = entryService;
        }

        public IActionResult Index()
        {
            return RedirectToAction(nameof(All));
        }

        public async Task<IActionResult> All()
        {
            var filter = new LogFilterViewModel
            {
                Start = DateTime.UtcNow.Date
            };

            var entries = await _entryService.GetEntriesByFilter(filter);
            var vm = new EntryListViewModel
            {
                MinDate = filter.Start,
                Entries = entries
            };

            return View("Index", vm);
        }

        public async Task<IActionResult> ForApplication(int applicationId)
        {
            var filter = new LogFilterViewModel
            {
                Start = DateTime.UtcNow.Date,
                ApplicationId = applicationId
            };

            var entries = await _entryService.GetEntriesByFilter(filter);
            var vm = new EntryListViewModel
            {
                ApplicationId = applicationId,
                MinDate = filter.Start,
                Entries = entries
            };

            return View("Index", vm);
        }

        public async Task<IActionResult> ForCategory(int categoryId)
        {
            var filter = new LogFilterViewModel
            {
                Start = DateTime.UtcNow.Date,
                CategoryId = categoryId
            };

            var entries = await _entryService.GetEntriesByFilter(filter);
            var vm = new EntryListViewModel
            {
                CategoryId = categoryId,
                MinDate = filter.Start,
                Entries = entries
            };

            return View("Index", vm);
        }

        [HttpPost]
        public async Task<IActionResult> Filter([FromBody] LogFilterViewModel filter)
        {
            var entries = await _entryService.GetEntriesByFilter(filter);
            return PartialView("_ListPartial", entries);
        }

        public async Task<IActionResult> Exception(int id)
        {
            var exception = await _entryService.GetException(id);
            if (exception == null)
            {
                return NoContent();
            }

            return PartialView("_LogExceptionPartial", exception);
        }
    }
}
