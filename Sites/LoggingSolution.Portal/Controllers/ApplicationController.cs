﻿using System.Linq;
using System.Threading.Tasks;
using LoggingSolution.Data;
using LoggingSolution.Portal.Dtos;
using LoggingSolution.Portal.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LoggingSolution.Portal.Controllers
{
    public class ApplicationController : Controller
    {
        private readonly LoggingDBContext _context;

        public ApplicationController(LoggingDBContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var applications = await _context.LogApplications
                .Select(x => new ApplicationDto
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToListAsync();

            var vm = new ApplicationListViewModel
            {
                Applications = applications
            };

            return View("List", vm);
        }
    }
}
