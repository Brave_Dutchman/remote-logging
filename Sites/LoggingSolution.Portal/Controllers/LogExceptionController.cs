﻿using LoggingSolution.Data;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LoggingSolution.Portal.Controllers
{
    public class LogExceptionController : Controller
    {
        private readonly LoggingDBContext _context;

        public LogExceptionController(LoggingDBContext context)
        {
            _context = context;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
    }
}
