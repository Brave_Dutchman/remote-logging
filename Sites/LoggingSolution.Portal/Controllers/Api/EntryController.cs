﻿using System.Threading.Tasks;
using LoggingSolution.Portal.Services;
using Microsoft.AspNetCore.Mvc;

namespace LoggingSolution.Portal.Controllers.Api
{
    [ApiController]
    [Route("Api/Entries")]
    public class EntryController : Controller
    {
        private readonly LogApiEntryService _entryService;

        public EntryController(LogApiEntryService entryService)
        {
            _entryService = entryService;
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _entryService.GetAsync(id);

            if (result == null)
            {
                return NoContent();
            }

            return Json(result, new Newtonsoft.Json.JsonSerializerSettings() { Formatting = Newtonsoft.Json.Formatting.Indented});
        }
    }
}
