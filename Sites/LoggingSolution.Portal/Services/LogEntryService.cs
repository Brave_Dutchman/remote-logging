﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingSolution.Data;
using LoggingSolution.Portal.Dtos;
using LoggingSolution.Portal.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace LoggingSolution.Portal.Services
{
    public class LogEntryService
    {
        private readonly LoggingDBContext _context;

        public LogEntryService(LoggingDBContext context)
        {
            _context = context;
        }

        public async Task<List<EntryDto>> GetEntriesByFilter(LogFilterViewModel filter)
        {
            var query = _context.LogEntries.AsQueryable();
            var entries = await _filterBy(query, filter)
                .Select(x => new EntryDto
                {
                    Id = x.Id,
                    DataId = x.Data.Id,
                    LogLevel = x.LogLevel,
                    LoggedOnUtc = x.LoggedOnUtc,
                    Message = x.Message.Message,
                    AdditionalInfo = x.AdditionalInfo,
                    EventInfo = new EventInfoDto
                    {
                        Id = x.Data.EventId,
                        Name = x.Data.EventName
                    },
                    Category = new CategoryDto
                    {
                        Id = x.Data.Category.Id,
                        Name = x.Data.Category.Name
                    },
                    Exception = x.Exception == null ? null : new ExceptionDetailDto
                    {
                        Id = x.Exception.Id,
                        Name = x.Exception.Name,
                        Message = x.Exception.Message
                    }
                })
                .ToListAsync();

            return entries;
        }

        public async Task<ExceptionDto> GetException(int id)
        {
            var exception = await _context.LogExceptions
                .Select(x => new ExceptionDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    StackTrace = x.StackTrace,
                    Message = x.Message,
                    Data = x.Data,
                    InnerException = x.InnerException == null ? null : new ExceptionDetailDto
                    {
                        Id = x.InnerException.Id,
                        Name = x.InnerException.Name,
                        Message = x.InnerException.Message
                    },
                })
            .FirstOrDefaultAsync(x => x.Id == id);

            return exception;
        }

        public IQueryable<LogEntry> _filterBy(IQueryable<LogEntry> logEntries, LogFilterViewModel filterModel)
        {
            if (logEntries == null)
            {
                throw new System.ArgumentNullException(nameof(logEntries));
            }

            if (filterModel == null)
            {
                return logEntries;
            }

            if (filterModel.ApplicationId.HasValue)
            {
                logEntries = logEntries.Where(x => x.Data.Category.ApplicationId == filterModel.ApplicationId.Value);
            }
            else if (filterModel.CategoryId.HasValue)
            {
                logEntries = logEntries.Where(x => x.Data.CategoryId == filterModel.CategoryId.Value);
            }

            if (filterModel.LogLevel.HasValue)
            {
                logEntries = logEntries.Where(x => x.LogLevel == filterModel.LogLevel.Value);
            }
            else if (filterModel.MinLogLevel.HasValue)
            {
                logEntries = logEntries.Where(x => x.LogLevel >= filterModel.MinLogLevel.Value);
            }

            if (filterModel.Start.HasValue)
            {
                var utc = filterModel.Start.Value.ToUniversalTime();
                var ticks = utc.Ticks;
                logEntries = logEntries.Where(x => x.LoggedOnUtc >= ticks);
            }

            if (filterModel.End.HasValue)
            {
                var utc = filterModel.End.Value.ToUniversalTime();
                var ticks = utc.Ticks;
                logEntries = logEntries.Where(x => x.LoggedOnUtc <= ticks);
            }

            if (filterModel.EventId.HasValue)
            {
                logEntries = logEntries.Where(x => x.Data.EventId == filterModel.EventId.Value);
            }

            if (!string.IsNullOrWhiteSpace(filterModel.Message))
            {
                logEntries = logEntries.Where(x => x.Message.Message.Contains(filterModel.Message));
            }

            if (!string.IsNullOrWhiteSpace(filterModel.AdditionalInfo))
            {
                logEntries = logEntries.Where(x => x.AdditionalInfo.Contains(filterModel.AdditionalInfo));
            }

            switch (filterModel.Order)
            {
                case SortOrder.NewestFirst:
                    return logEntries.OrderByDescending(x => x.LoggedOnUtc);
                case SortOrder.OldestFirst:
                    return logEntries.OrderBy(x => x.LoggedOnUtc);
                default:
                    return logEntries;
            }
        }
    }
}
