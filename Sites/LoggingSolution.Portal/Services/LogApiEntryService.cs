﻿using System.Linq;
using System.Threading.Tasks;
using LoggingSolution.Data;
using LoggingSolution.Portal.Dtos;
using LoggingSolution.Portal.Dtos.Api;
using Microsoft.EntityFrameworkCore;

namespace LoggingSolution.Portal.Services
{
    public class LogApiEntryService
    {
        private readonly LoggingDBContext _context;

        public LogApiEntryService(LoggingDBContext context)
        {
            _context = context;
        }

        public async Task<ApiEventDto> GetAsync(int id)
        {
            var entry = await _context.LogEntries
                .Select(x => new ApiEventDto
                {
                    Id = x.Id,
                    LogLevel = x.LogLevel,
                    LoggedOnUtc = x.LoggedOnUtc,
                    Message = x.Message.Message,
                    AdditionalInfo = x.AdditionalInfo,
                    EventInfo = new EventInfoDto
                    {
                        Id = x.Data.EventId,
                        Name = x.Data.EventName
                    },
                    Category = new ApiCategoryDto
                    {
                        Id = x.Data.Category.Id,
                        Name = x.Data.Category.Name,
                        Application = new ApiApplicationDto
                        {
                            Id = x.Data.Category.Application.Id,
                            Name = x.Data.Category.Application.Name
                        }
                    },
                    ExceptionGroupId = x.Exception == null ? null : (int?) x.Exception.GroupId
                })
            .FirstOrDefaultAsync(x => x.Id == id);

            if (entry == null)
            {
                return null;
            }

            if (entry.ExceptionGroupId.HasValue)
            {
                entry.Exception = await GetExceptionsAsync(entry.ExceptionGroupId.Value);
            }

            return entry;
        }

        private async Task<ApiExceptionDto> GetExceptionsAsync(int exceptionGroupId)
        {
            var exceptions = await _context.LogExceptionGroups
                .Where(x => x.Id == exceptionGroupId)
                .SelectMany(x => x.Exceptions)
                .OrderByDescending(x => x.Id)
                .Select(x => new ApiExceptionDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    StackTrace = x.StackTrace,
                    Message = x.Message,
                    Data = x.Data,
                    InnerExceptionId = x.InnerExceptionId
                })
                .ToListAsync();

            var outer = exceptions.First();

            foreach (var exception in exceptions)
            {
                if (exception.InnerExceptionId.HasValue)
                {
                    exception.InnerException = exceptions
                        .FirstOrDefault(x => x.Id == exception.InnerExceptionId.Value);
                }
            }

            return outer;
        }
    }
}
