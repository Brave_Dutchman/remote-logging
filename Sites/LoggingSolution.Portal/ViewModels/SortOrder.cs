﻿namespace LoggingSolution.Portal.ViewModels
{
    public enum SortOrder
    {
        NewestFirst = 0,
        OldestFirst = 1
    }
}