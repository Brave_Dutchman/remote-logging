﻿using System.Collections.Generic;
using LoggingSolution.Portal.Dtos;

namespace LoggingSolution.Portal.ViewModels
{
    public class CategoryListViewModel
    {
        public IList<CategoryListDto> Categories { get; set; }
    }
}