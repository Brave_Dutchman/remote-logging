﻿using System.Collections.Generic;
using LoggingSolution.Portal.Dtos;

namespace LoggingSolution.Portal.ViewModels
{
    public class CategoryListByAppViewModel
    {
        public int ApplicationId { get; set; }
        public IList<CategoryDto> Categories { get; set; }
    }
}