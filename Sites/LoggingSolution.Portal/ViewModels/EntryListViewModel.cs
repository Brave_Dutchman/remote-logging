﻿using System;
using System.Collections.Generic;
using LoggingSolution.Portal.Dtos;

namespace LoggingSolution.Portal.ViewModels
{
    public class EntryListViewModel
    {
        public int? CategoryId { get; set; }
        public int? ApplicationId { get; set; }

        public DateTime? MinDate { get; set; }

        public IList<EntryDto> Entries { get; set; }
    }
}