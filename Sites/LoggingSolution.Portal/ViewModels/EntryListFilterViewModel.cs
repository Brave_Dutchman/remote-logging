﻿using System.Collections.Generic;
using LoggingSolution.Portal.Dtos;

namespace LoggingSolution.Portal.ViewModels
{
    public class EntryListFilterViewModel
    {
        public LogFilterViewModel Filter { get; set; }

        public IList<EntryDto> Entries { get; set; }
    }
}