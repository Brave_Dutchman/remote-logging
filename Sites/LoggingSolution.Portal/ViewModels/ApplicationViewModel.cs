﻿using System.Collections.Generic;
using LoggingSolution.Portal.Dtos;

namespace LoggingSolution.Portal.ViewModels
{
    public class ApplicationListViewModel
    {
        public IList<ApplicationDto> Applications { get; set; }
    }
}