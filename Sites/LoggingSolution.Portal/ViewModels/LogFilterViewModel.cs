﻿using System;
using Microsoft.Extensions.Logging;

namespace LoggingSolution.Portal.ViewModels
{
    public class LogFilterViewModel
    {
        public int? CategoryId { get; set; }
        public int? ApplicationId { get; set; }
        
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }

        public LogLevel? MinLogLevel { get; set; }
        public LogLevel? LogLevel { get; set; }

        public int? EventId { get; set; }

        public string Message { get; set; }

        public string AdditionalInfo { get; set; }

        public SortOrder Order { get; set; }
    }
}