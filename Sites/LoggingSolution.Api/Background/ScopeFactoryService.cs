﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace LoggingSolution.Api.Background
{
    public class ScopeFactoryService
    {
        private readonly IServiceProvider _rootProvider;

        public ScopeFactoryService(IServiceProvider rootProvider)
        {
            _rootProvider = rootProvider;
        }

        public IScopedServiceProvider CreateScope()
        {
            return new ServiceScope(_rootProvider.CreateScope());
        }
    }
}
