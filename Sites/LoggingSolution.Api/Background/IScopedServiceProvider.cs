﻿using System;

namespace LoggingSolution.Api.Background
{
    public interface IScopedServiceProvider : IDisposable
    {
        T GetService<T>();

        T GetRequiredService<T>();
    }
}
