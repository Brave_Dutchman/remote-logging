﻿using System.Linq;
using System.Threading.Tasks;
using LoggingSolution.Api.Messaging;

namespace LoggingSolution.Api.Background
{
    public class MessagingService
    {
        private readonly HubContextService _hubContext;
        private readonly ConnectionInfoCollection _connectionInfos;

        public MessagingService(HubContextService hubContext, ConnectionInfoCollection connectionInfos)
        {
            _hubContext = hubContext;
            _connectionInfos = connectionInfos;
        }

        public async Task Handle(EntryInfo entry)
        {
            var enumerable = _connectionInfos
                .Where(x => x.MinLogLevel >= entry.LogLevel && x.Applications.Contains(entry.ApplicationId));

            foreach (var connectionInfo in enumerable)
            {
                await _hubContext.Client(connectionInfo.ConnectionId).SendAsync(nameof(EntryInfo), entry);
            }
        }
    }
}
