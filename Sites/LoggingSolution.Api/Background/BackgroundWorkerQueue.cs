﻿using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace LoggingSolution.Api.Background
{
    public class BackgroundWorkerQueue
    {
        private readonly ConcurrentQueue<int> _queue;
        private readonly SemaphoreSlim _semaphoreSlim;

        public BackgroundWorkerQueue()
        {
            _semaphoreSlim = new SemaphoreSlim(0);
            _queue = new ConcurrentQueue<int>();
        }

        public void Add(int entryId)
        {
            if (entryId <= 0)
            {
                return;
            }

            _queue.Enqueue(entryId);
            _semaphoreSlim.Release();
        }

        public async Task<int> DequeueAsync()
        {
            await _semaphoreSlim.WaitAsync();

            _queue.TryDequeue(out var entryId);
            return entryId;
        }
    }
}
