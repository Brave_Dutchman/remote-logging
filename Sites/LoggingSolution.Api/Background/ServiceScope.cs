﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace LoggingSolution.Api.Background
{
    internal class ServiceScope : IScopedServiceProvider
    {
        private bool _disposed;

        private readonly IServiceScope _serviceScope;

        public ServiceScope(IServiceScope serviceScope)
        {
            _serviceScope = serviceScope;
        }

        public T GetRequiredService<T>()
        {
            if (_disposed)
            {
                throw new InvalidOperationException("Resource is disposed");
            }

            return _serviceScope.ServiceProvider.GetRequiredService<T>();
        }

        public T GetService<T>()
        {
            if (_disposed)
            {
                throw new InvalidOperationException("Resource is disposed");
            }

            return _serviceScope.ServiceProvider.GetService<T>();
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                _serviceScope.Dispose();
                _disposed = true;
            }
        }
    }
}
