﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LoggingSolution.Api.Messaging;
using LoggingSolution.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace LoggingSolution.Api.Background
{
    public class LoggingBackgroundWorker : BackgroundService
    {
        private readonly BackgroundWorkerQueue _workerQueue;
        private readonly ScopeFactoryService _scopeFactory;

        public LoggingBackgroundWorker(BackgroundWorkerQueue workerQueue, ScopeFactoryService scopeFactory)
        {
            _workerQueue = workerQueue;
            _scopeFactory = scopeFactory;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var entryId = await _workerQueue.DequeueAsync();
                if (entryId == default)
                {
                    continue;
                }

                using (var scope = _scopeFactory.CreateScope())
                {
                    var context = scope.GetRequiredService<LoggingDBContext>();

                    var entry = await context.LogEntries
                        .Where(x => x.Id == entryId)
                        .Select(x => new EntryInfo
                        {
                            Id = x.Id,
                            ApplicationId = x.Data.Category.ApplicationId,
                            EventId = x.Data.EventId,
                            CategoryId = x.Data.CategoryId,
                            LogLevel = x.LogLevel,
                            HasMessage = x.Message != null,
                            HasException = x.Exception != null,
                        })
                        .FirstOrDefaultAsync();

                    if (entry == null)
                    {
                        //TODO log
                        continue;
                    }

                    var messagingService = scope.GetRequiredService<MessagingService>();
                    await messagingService.Handle(entry);
                }
            }
        }
    }
}
