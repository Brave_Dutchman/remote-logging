﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace LoggingSolution.Api.Messaging.Internal
{
    internal class ClientProxy : Messaging.IClientProxy
    {
        private readonly Microsoft.AspNetCore.SignalR.IClientProxy _clientProxy;

        public ClientProxy(Microsoft.AspNetCore.SignalR.IClientProxy clientProxy)
        {
            _clientProxy = clientProxy;
        }

        public async Task SendAsync(string method, CancellationToken token = default)
        {
            await _clientProxy.SendAsync(method, token);
        }

        public async Task SendAsync<T1>(string method, T1 param, CancellationToken token = default)
        {
            await _clientProxy.SendAsync(method, param, token);
        }

        public async Task SendAsync<T1, T2>(string method, T1 param1, T2 param2, CancellationToken token = default)
        {
            await _clientProxy.SendAsync(method, param1, param2, token);
        }
    }
}
