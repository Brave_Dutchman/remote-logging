﻿using Microsoft.Extensions.Logging;

namespace LoggingSolution.Api.Messaging
{
    public class EntryInfo
    {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public int EventId { get; set; }
        public int CategoryId { get; set; }
        public LogLevel LogLevel { get; set; }
        public bool HasMessage { get; set; }
        public bool HasException { get; set; }
    }
}
