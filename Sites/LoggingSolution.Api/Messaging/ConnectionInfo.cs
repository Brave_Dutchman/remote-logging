﻿using Microsoft.Extensions.Logging;

namespace LoggingSolution.Api.Messaging
{
    public class ConnectionInfo
    {
        public ConnectionInfo(string connectionId)
        {
            ConnectionId = connectionId;
        }

        public string ConnectionId { get; }

        public LogLevel MinLogLevel { get; set; }

        public int[] Applications { get; set; }
    }
}
