﻿using System.Collections;
using System.Collections.Generic;

namespace LoggingSolution.Api.Messaging
{
    public class ConnectionInfoCollection : IEnumerable<ConnectionInfo>
    {
        private readonly Dictionary<string, ConnectionInfo> _connectionInfos;

        public ConnectionInfoCollection()
        {
            _connectionInfos = new Dictionary<string, ConnectionInfo>();
        }

        public ConnectionInfo this[string connectionId]
        {
            get { return _connectionInfos[connectionId]; }
        }

        public void Add(string connectionId, InitializationData data)
        {
            ConnectionInfo info = new ConnectionInfo(connectionId)
            {
                MinLogLevel = data.MinLogLevel
            };

            _connectionInfos[connectionId] = info;
        }

        public void Remove(string connectionId)
        {
            _connectionInfos.Remove(connectionId);
        }

        public IEnumerator<ConnectionInfo> GetEnumerator()
        {
            return _connectionInfos.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
