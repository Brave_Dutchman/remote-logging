﻿using System.Threading.Tasks;
using LoggingSolution.Api.Messaging.Internal;

namespace LoggingSolution.Api.Messaging
{
    public class HubContextService
    {
        private readonly Microsoft.AspNetCore.SignalR.IHubContext<EventsHub> _hubContext;

        public HubContextService(Microsoft.AspNetCore.SignalR.IHubContext<EventsHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public IClientProxy Client(string connectionId)
        {
            return Wrap(_hubContext.Clients.Client(connectionId));
        }

        public IClientProxy Group(string name)
        {
            return Wrap(_hubContext.Clients.Group(name));
        }

        private IClientProxy Wrap(Microsoft.AspNetCore.SignalR.IClientProxy clientProxy)
        {
            return new ClientProxy(clientProxy);
        }

        public async Task JoinGroup(string groupName, string connectionId)
        {
            await _hubContext.Groups.AddToGroupAsync(connectionId, groupName);
        }

        public async Task LeaveGroup(string groupName, string connectionId)
        {
            await _hubContext.Groups.RemoveFromGroupAsync(connectionId, groupName);
        }
    }
}
