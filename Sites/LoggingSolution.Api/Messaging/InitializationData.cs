﻿using Microsoft.Extensions.Logging;

namespace LoggingSolution.Api.Messaging
{
    public class InitializationData
    {
        public LogLevel MinLogLevel { get; set; }
    }
}
