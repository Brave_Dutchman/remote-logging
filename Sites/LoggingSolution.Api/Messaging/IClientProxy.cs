﻿using System.Threading;
using System.Threading.Tasks;

namespace LoggingSolution.Api.Messaging
{
    public interface IClientProxy
    {
        Task SendAsync(string method, CancellationToken token = default);
        Task SendAsync<T1, T2>(string method, T1 param1, T2 param2, CancellationToken token = default);
        Task SendAsync<T1>(string method, T1 param, CancellationToken token = default);
    }
}
