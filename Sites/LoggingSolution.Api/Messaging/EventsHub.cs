﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace LoggingSolution.Api.Messaging
{
    public class EventsHub : Hub
    {
        private readonly ConnectionInfoCollection _connectionInfo;

        public EventsHub(ConnectionInfoCollection connectionInfo)
        {
            _connectionInfo = connectionInfo;
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _connectionInfo.Remove(Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }

        public async Task InitializeAsync(InitializationData data)
        {
            var result = new OnInitialized { Result = "Ok" };
            if (data == null)
            {
                result.Result = "Failed";
            }
            else
            {
                _connectionInfo.Add(Context.ConnectionId, data);
            }
            
            await Clients.Client(Context.ConnectionId).SendAsync(nameof(OnInitialized), result);
        }
    }
}
