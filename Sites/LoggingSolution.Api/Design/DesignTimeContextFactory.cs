﻿using LoggingSolution.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace LoggingSolution.Api.Design
{
    public class DesignTimeContextFactory : IDesignTimeDbContextFactory<LoggingDBContext>
    {
        public LoggingDBContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<LoggingDBContext>();
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=LoggingDb;Trusted_Connection=True;");
            
            return new LoggingDBContext(optionsBuilder.Options);
        }
    }
}
