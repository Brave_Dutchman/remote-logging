﻿using LoggingSolution.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace LoggingSolution.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().MigrateDatabase<LoggingDBContext>().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
