﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace LoggingSolution.Api
{
    public static class IWebHostExtentions
    {
        public static IWebHost MigrateDatabase<T>(this IWebHost webHost)
            where T : DbContext
        {

            using (var scope = webHost.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<T>();
                context.Database.Migrate();
            }

            return webHost;
        }
    }
}
