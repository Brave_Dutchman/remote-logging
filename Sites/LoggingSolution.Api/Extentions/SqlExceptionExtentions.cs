﻿using System;
using System.Data.SqlClient;

namespace LoggingSolution.Api.Controllers
{
    public static class SqlExceptionExtentions
    {
        public static bool IsUniqueValidation(this Exception ex)
        {
            return ex.GetBaseException() is SqlException sqlException &&
                IsUniqueValidation(sqlException);
        }

        public static bool IsUniqueValidation(this SqlException sqlException)
        {
            switch (sqlException.Number)
            {
                case 547: // Constraint check violation
                case 2601: // Duplicated key row error
                case 2627: // Unique constraint error
                    return true;
                default:
                    return false;
            }
        }
    }
}
