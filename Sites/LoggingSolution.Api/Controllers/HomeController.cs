﻿using Microsoft.AspNetCore.Mvc;

namespace LoggingSolution.Api.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Ok("Live");
        }
    }
}
