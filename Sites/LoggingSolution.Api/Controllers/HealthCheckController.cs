﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LoggingSolution.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class HealthCheckController : Controller
    {
        [Route("")]
        public IActionResult Index()
        {
            return Ok();
        }
    }
}
