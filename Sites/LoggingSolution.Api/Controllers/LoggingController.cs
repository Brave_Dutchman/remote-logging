﻿using System;
using System.Threading.Tasks;
using LoggingSolution.Api.Background;
using LoggingSolution.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace LoggingSolution.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoggingController : ControllerBase
    {
        private readonly LoggingDBContext _context;
        private readonly BackgroundWorkerQueue _backgroundWorkerQueue;
        private readonly ILogger<LoggingController> _logger;

        public LoggingController(LoggingDBContext context, BackgroundWorkerQueue backgroundWorkerQueue, ILogger<LoggingController> logger)
        {
            _context = context;
            _backgroundWorkerQueue = backgroundWorkerQueue;
            _logger = logger;
        }

        [HttpPost("log")]
        public async Task<IActionResult> Log(LogPostItem item)
        {
            var application = await GetApplication(item);
            var category = await GetCategory(item, application);
            var data = await GetData(item, application, category);
            var message = await GetMessage(item, application);
            var exception = await GetException(item, application);

            var model = new LogEntry
            {
                LogLevel = (LogLevel)item.LogLevel,
                Data = data,
                LoggedOnUtc = item.LoggedOnUtc,
                Exception = exception,
                Message = message,
                AdditionalInfo = item.AdditionalInfo
            };

            _context.Add(model);
            await _context.SaveChangesAsync();

            _backgroundWorkerQueue.Add(model.Id);

            return Ok();
        }

        private async Task<LogApplication> GetApplication(LogPostItem item)
        {
            var applicationName = item.LoggerInfo.ApplicationName;

            var found = await _context.LogApplications.FirstOrDefaultAsync(x => x.Name == applicationName);
            if (found != null)
            {
                return found;
            }

            LogApplication model = new LogApplication { Name = applicationName };
            _context.Add(model);
            await _context.SaveChangesAsync();

            try
            {
                _context.Add(model);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (!ex.IsUniqueValidation())
                {
                    _logger.LogError(ex, "Exception while saving");
                    throw;
                }

                found = await _context.LogApplications.FirstOrDefaultAsync(x => x.Name == applicationName);
                if (found == null)
                {
                    _logger.LogError(ex, "Exception while fetching data after a {name} exception", nameof(SqlExceptionExtentions.IsUniqueValidation));
                    throw;
                }

                return found;
            }

            return model;
        }

        private async Task<LogCategory> GetCategory(LogPostItem item, LogApplication application)
        {
            var categoryName = item.LoggerInfo.CategoryName;

            var found = await _context.LogCategories.FirstOrDefaultAsync(x => x.Name == categoryName && x.ApplicationId == application.Id);
            if (found != null)
            {
                return found;
            }

            LogCategory model = new LogCategory
            {
                Name = categoryName,
                Application = application
            };

            try
            {
                _context.Add(model);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (!ex.IsUniqueValidation())
                {
                    _logger.LogError(ex, "Exception while saving");
                    throw;
                }

                found = await _context.LogCategories.FirstOrDefaultAsync(x => x.Name == categoryName);
                if (found == null)
                {
                    _logger.LogError(ex, "Exception while fetching data after a {name} exception", nameof(SqlExceptionExtentions.IsUniqueValidation));
                    throw;
                }

                return found;
            }

            return model;
        }

        private async Task<LogData> GetData(LogPostItem item, LogApplication application, LogCategory category)
        {
            var hash = Hasher.Hash(string.Concat(item.EventId, item.EventName ?? string.Empty, application.Id, category.Id));

            var found = await _context.LogDatas.FirstOrDefaultAsync(x => x.Hash == hash);
            if (found != null)
            {
                return found;
            }

            var model = new LogData
            {
                Hash = hash,
                EventId = item.EventId,
                EventName = item.EventName,
                CategoryId = category.Id
            };

            try
            {
                _context.Add(model);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (!ex.IsUniqueValidation())
                {
                    _logger.LogError(ex, "Exception while saving");
                    throw;
                }

                found = await _context.LogDatas.FirstOrDefaultAsync(x => x.Hash == hash);
                if (found == null)
                {
                    _logger.LogError(ex, "Exception while fetching data after a {name} exception", nameof(SqlExceptionExtentions.IsUniqueValidation));
                    throw;
                }

                return found;
            }

            return model;
        }
        
        private async Task<LogMessage> GetMessage(LogPostItem item, LogApplication application)
        {
            if (item.Message == null)
            {
                return null;
            }

            var hash = Hasher.Hash(item.Message);
            var found = await _context.LogMessages.FirstOrDefaultAsync(x => x.Hash == hash && x.ApplicationId == application.Id);
            if (found != null)
            {
                return found;
            }

            var model = new LogMessage
            {
                Hash = hash,
                Message = item.Message,
                Application = application
            };

            try
            {
                _context.Add(model);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (!ex.IsUniqueValidation())
                {
                    _logger.LogError(ex, "Exception while saving");
                    throw;
                }

                found = await _context.LogMessages.FirstOrDefaultAsync(x => x.Hash == hash);
                if (found == null)
                {
                    _logger.LogError(ex, "Exception while fetching data after a {name} exception", nameof(SqlExceptionExtentions.IsUniqueValidation));
                    throw;
                }

                return found;
            }

            return model;
        }

        private async Task<LogException> GetException(LogPostItem item, LogApplication application)
        {
            if (item.Exception == null)
            {
                return null;
            }

            var exData = item.Exception;
            var hash = Hasher.Hash(string.Concat(exData.Name, exData.Message, exData.Source, exData.StackTrace, exData.Data ?? string.Empty));

            var found = await _context.LogExceptions.FirstOrDefaultAsync(x => x.Hash == hash && x.ApplicationId == application.Id);
            if (found != null)
            {
                return found;
            }

            var group = new LogExceptionGroup();

            var model = new LogException
            {
                Hash = hash,
                Name = exData.Name,
                Message = exData.Message,
                StackTrace = exData.StackTrace,
                Data = exData.Data,
                Application = application,
                Group = group
            };

            var current = exData.InnerException;
            var currentModel = model;
            while (current != null)
            {
                var innerHash = Hasher.Hash(string.Concat(current.Name, current.Message, current.Source, current.StackTrace, current.Data ?? string.Empty));

                currentModel.InnerException = new LogException
                {
                    Hash = innerHash,
                    Name = current.Name,
                    Message = current.Message,
                    StackTrace = current.StackTrace,
                    Data = current.Data,
                    Application = application,
                    Group = group
                };

                currentModel = currentModel.InnerException;
                current = current.InnerException;
            }

            try
            {
                _context.Add(group);
                _context.Add(model);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (!ex.IsUniqueValidation())
                {
                    _logger.LogError(ex, "Exception while saving");
                    throw;
                }

                found = await _context.LogExceptions.FirstOrDefaultAsync(x => x.Hash == hash);
                if (found == null)
                {
                    _logger.LogError(ex, "Exception while fetching data after a {name} exception", nameof(SqlExceptionExtentions.IsUniqueValidation));
                    throw;
                }

                return found;
            }

            return model;
        }
    }
}
