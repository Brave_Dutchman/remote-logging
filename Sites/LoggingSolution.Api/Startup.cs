﻿using LoggingSolution.Api.Background;
using LoggingSolution.Api.Messaging;
using LoggingSolution.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LoggingSolution.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<LoggingDBContext>((optionsBuilder) =>
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("LoggingDatabase"));
            });

            services.AddSignalR();
            
            services.AddHostedService<LoggingBackgroundWorker>();

            services.AddSingleton<ConnectionInfoCollection>();
            services.AddSingleton<ScopeFactoryService>();
            services.AddSingleton<BackgroundWorkerQueue>();

            services.AddScoped<MessagingService>();
            services.AddScoped<HubContextService>();
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler();
            }

            app.UseHttpsRedirection();

            app.UseSignalR((c) => c.MapHub<EventsHub>("/events"));

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
