﻿using System;
using System.Collections.Generic;
using LoggingSolution;

namespace TestConsole
{
    public class TestEnhancer : ILogEnhancer
    {
        public void Enhance(Dictionary<string, object> keyValuePairs)
        {
            keyValuePairs.Add("Test", Guid.NewGuid().ToString());
        }
    }
}
