﻿using System;
using System.Threading.Tasks;
using LoggingSolution;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace TestConsole
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            var host = new HostBuilder()
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    IHostingEnvironment env = builderContext.HostingEnvironment;

                    config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                    config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<RemoteLoggerBackgroundService>();
                    services.TryAddEnumerable(ServiceDescriptor.Singleton<ILogEnhancer, TestEnhancer>());
                })
                .ConfigureLogging((hostContext, configLogging) =>
                {
                    var remoteSec = hostContext.Configuration.GetSection("RemoteLogging");
                    var loggingSec = hostContext.Configuration.GetSection("Logging");
                    configLogging.AddRemoteLogging(remoteSec, loggingSec);
                })
                .Build();

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Task.Run(async () =>
            {
                int count = 0;
                while (count < 10)
                {
                    await Task.Delay(5000);

                    var logger = host.Services.GetRequiredService<ILogger<Program>>();

                    try
                    {
                        throw new TestException("Not yet implemented :)");
                    }
                    catch (System.Exception ex)
                    {
                        try
                        {
                            throw new Exception("wrapper", ex);
                        }
                        catch (Exception ex2)
                        {
                            logger.LogError(ex2, "Test Exception");
                        }
                    }

                    Console.WriteLine("Logged Error");

                    count++;
                }
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

            await host.RunAsync();
        }
    }
}
